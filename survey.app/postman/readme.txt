All PostMan end points point to : survey-app.localhost.com

This can be configured to redirect to IP address of wherever the API server is running.
In case of APIs running locally, put following entry in C:\WINDOWS\SYSTEM32\drivers\etc\hosts file:

127.0.0.1   survey-app.localhost.com

If the APIs are running on some other machine, try to find it's IP address and replace the 127.0.0.1 with that address in your hosts file.