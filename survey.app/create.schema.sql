CREATE VIEW `responses_view` AS CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `responses_view` AS select `u`.`NAME` AS `sender`,`s`.`TITLE` AS `title`,`r`.`ID` AS `ID`,`r`.`CREATED_AT` AS `CREATED_AT`,`r`.`OWNER_ID` AS `OWNER_ID`,`r`.`REFERENCE_ID` AS `REFERENCE_ID`,`r`.`SURVEY_ID` AS `SURVEY_ID`,`r`.`STATUS` AS `STATUS` from ((`responses` `r` join `surveys` `s` on((`s`.`ID` = `r`.`SURVEY_ID`))) join `users` `u` on((`u`.`ID` = `r`.`REFERENCE_ID`)));

SELECT u.name as owner, s.title FROM surveys s
  INNER JOIN users u ON u.id = s.owner_id  CREATE TABLE `answers` (
  `ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ANSWER` varchar(2000) DEFAULT NULL,
  `RESPONSE_ID` int(11) unsigned NOT NULL,
  `QUESTION_ID` int(11) unsigned NOT NULL,
  `SURVEY_ID` int(11) unsigned NOT NULL,
  `CREATED_AT` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `ID_UNIQUE` (`ID`),
  UNIQUE KEY `RESPONSE_QUESTION_ID_idx` (`RESPONSE_ID`,`QUESTION_ID`),
  KEY `ANSWER_RESPONSE_ID_idx` (`RESPONSE_ID`),
  KEY `ANSWER_QUESTION_ID_idx` (`QUESTION_ID`,`SURVEY_ID`),
  CONSTRAINT `ANSWER_QUESTION_ID` FOREIGN KEY (`QUESTION_ID`, `SURVEY_ID`) REFERENCES `questions` (`ID`, `SURVEY_ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `ANSWER_RESPONSE_ID` FOREIGN KEY (`RESPONSE_ID`) REFERENCES `responses` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

CREATE TABLE `questions` (
  `ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `SURVEY_ID` int(11) unsigned NOT NULL,
  `TITLE` varchar(2000) NOT NULL,
  `HELPTEXT` varchar(2000) DEFAULT NULL,
  `TYPE` int(10) NOT NULL,
  `QUESTION_INDEX` int(10) unsigned DEFAULT NULL,
  `CREATED_AT` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `ID_UNIQUE` (`ID`),
  UNIQUE KEY `QUESTIONNAIRE_QUESTION_ID` (`ID`,`SURVEY_ID`),
  KEY `QUESTION_QUESTIONNAIRE_ID_idx` (`SURVEY_ID`),
  CONSTRAINT `QUESTION_SURVEY_ID` FOREIGN KEY (`SURVEY_ID`) REFERENCES `surveys` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

CREATE TABLE `responses` (
  `ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `CREATED_AT` datetime DEFAULT CURRENT_TIMESTAMP,
  `OWNER_ID` int(11) unsigned NOT NULL,
  `REFERENCE_ID` int(11) unsigned NOT NULL,
  `SURVEY_ID` int(11) unsigned NOT NULL,
  `STATUS` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `ID_UNIQUE` (`ID`),
  KEY `OWNER_ID_idx` (`OWNER_ID`),
  KEY `REFERENCE_ID_idx` (`REFERENCE_ID`),
  KEY `RESPONSE_QUESTIONNAIRE_ID_idx` (`SURVEY_ID`),
  CONSTRAINT `RESPONSE_SURVEY_ID` FOREIGN KEY (`SURVEY_ID`) REFERENCES `surveys` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

CREATE TABLE `surveys` (
  `ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `TITLE` varchar(2000) NOT NULL,
  `CREATED_AT` datetime DEFAULT CURRENT_TIMESTAMP,
  `OWNER_ID` int(11) unsigned NOT NULL,
  `HELPTEXT` varchar(2000) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `ID_UNIQUE` (`ID`),
  KEY `OWNER_ID_idx` (`OWNER_ID`),
  CONSTRAINT `QUESTIONNAIRE_OWNER_ID` FOREIGN KEY (`OWNER_ID`) REFERENCES `users` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

CREATE TABLE `users` (
  `ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `NAME` varchar(45) NOT NULL,
  `LOGIN` varchar(45) NOT NULL,
  `PASSWORD` varchar(2000) NOT NULL,
  `IS_ADMIN` smallint(1) NOT NULL,
  `CREATED_AT` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `LOGIN_UNIQUE` (`LOGIN`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
