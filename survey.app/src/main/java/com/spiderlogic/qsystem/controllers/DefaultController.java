package com.spiderlogic.qsystem.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(value="/")
public class DefaultController {
	 
	 @RequestMapping()
	 public String defaultMapping() {
		 return "/app/index.html";
	 }
}
