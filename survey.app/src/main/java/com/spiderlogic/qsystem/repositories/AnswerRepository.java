package com.spiderlogic.qsystem.repositories;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.spiderlogic.qsystem.domain.Answer;
import com.spiderlogic.qsystem.domain.Survey;
import com.spiderlogic.qsystem.domain.User;

@RepositoryRestResource(collectionResourceRel = "answers", path = "answers")
public interface AnswerRepository extends PagingAndSortingRepository<Answer, Long> {

	List<Answer> findById(@Param("id") long id);
	List<Answer> findByResponseId(@Param("responseId") long responseId);

}
