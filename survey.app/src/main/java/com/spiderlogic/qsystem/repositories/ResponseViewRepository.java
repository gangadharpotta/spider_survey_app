package com.spiderlogic.qsystem.repositories;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.spiderlogic.qsystem.domain.ResponseView;

@RepositoryRestResource(collectionResourceRel = "responseviews", path = "responseviews")
public interface ResponseViewRepository extends PagingAndSortingRepository<ResponseView, Long> {

	List<ResponseView> findById(@Param("id") long id);
	List<ResponseView> findByOwnerId(@Param("ownerId") long ownerId);
	List<ResponseView> findByReferenceId(@Param("referenceId") long referenceId);
	List<ResponseView> findBySurveyId(@Param("surveyId") long surveyId);

}
