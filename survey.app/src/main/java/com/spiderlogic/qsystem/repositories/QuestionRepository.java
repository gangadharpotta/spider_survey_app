package com.spiderlogic.qsystem.repositories;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.spiderlogic.qsystem.domain.Question;

@RepositoryRestResource(collectionResourceRel = "questions", path = "questions")
public interface QuestionRepository extends PagingAndSortingRepository<Question, Long> {

	List<Question> findById(@Param("id") long id);
	List<Question> findBySurveyId(@Param("surveyId") long surveyId);

}
