package com.spiderlogic.qsystem.repositories;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.spiderlogic.qsystem.domain.Survey;
import com.spiderlogic.qsystem.domain.Response;

@RepositoryRestResource(collectionResourceRel = "responses", path = "responses")
public interface ResponseRepository extends PagingAndSortingRepository<Response, Long> {

	List<Response> findById(@Param("id") long id);
	List<Response> findByOwnerId(@Param("ownerId") long ownerId);
	List<Response> findByReferenceId(@Param("referenceId") long referenceId);
	List<Response> findBySurveyId(@Param("surveyId") long surveyId);

}
