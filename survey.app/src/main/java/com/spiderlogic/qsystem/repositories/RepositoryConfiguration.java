package com.spiderlogic.qsystem.repositories;

import java.net.URI;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.rest.core.config.RepositoryRestConfiguration;
import org.springframework.data.rest.webmvc.config.RepositoryRestMvcConfiguration;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping;

import com.spiderlogic.qsystem.domain.Answer;
import com.spiderlogic.qsystem.domain.AnswerView;
import com.spiderlogic.qsystem.domain.Question;
import com.spiderlogic.qsystem.domain.Response;
import com.spiderlogic.qsystem.domain.ResponseView;
import com.spiderlogic.qsystem.domain.Survey;
import com.spiderlogic.qsystem.domain.SurveyView;
import com.spiderlogic.qsystem.domain.User;

@Configuration
public class RepositoryConfiguration extends RepositoryRestMvcConfiguration {
 
    @Override
    protected void configureRepositoryRestConfiguration(RepositoryRestConfiguration config) {
        config.exposeIdsFor(User.class);
        config.exposeIdsFor(Survey.class);
        config.exposeIdsFor(SurveyView.class);
        config.exposeIdsFor(Question.class);
        config.exposeIdsFor(Response.class);
        config.exposeIdsFor(ResponseView.class);
        config.exposeIdsFor(Answer.class);
        config.exposeIdsFor(AnswerView.class);
        config.setBaseUri(URI.create("/rest"));
    }

	@Override
	public RequestMappingHandlerMapping repositoryExporterHandlerMapping() {
		RequestMappingHandlerMapping mappings = super.repositoryExporterHandlerMapping();
		return mappings;
	}
 
}