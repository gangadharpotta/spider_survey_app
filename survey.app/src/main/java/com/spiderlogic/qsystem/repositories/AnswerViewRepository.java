package com.spiderlogic.qsystem.repositories;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.spiderlogic.qsystem.domain.AnswerView;

@RepositoryRestResource(collectionResourceRel = "answerviews", path = "answerviews")
public interface AnswerViewRepository extends PagingAndSortingRepository<AnswerView, Long> {

	List<AnswerView> findById(@Param("id") long id);
	List<AnswerView> findByResponseId(@Param("responseId") long responseId);

}
