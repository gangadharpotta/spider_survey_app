package com.spiderlogic.qsystem.repositories;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.spiderlogic.qsystem.domain.SurveyView;

@RepositoryRestResource(collectionResourceRel = "surveyviews", path = "surveyviews")
public interface SurveyViewRepository extends PagingAndSortingRepository<SurveyView, Long> {

	List<SurveyView> findById(@Param("id") long id);
	List<SurveyView> findByOwnerId(@Param("ownerId") long ownerId);
	List<SurveyView> findByOwnerName(@Param("ownerName") long ownerName);

}
