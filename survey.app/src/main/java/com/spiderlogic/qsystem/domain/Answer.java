package com.spiderlogic.qsystem.domain;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity 
@Table (name = "answers")
public class Answer {

	@Id
	@Column(nullable = false, name = "ID")	
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	
	@Column(nullable = true, name = "CREATED_AT", updatable=false, insertable=false)
	private Date createdAt;

	@Column(nullable = false, name = "ANSWER")
	private String answer;

	@Column(nullable = false, name = "RESPONSE_ID", updatable=false, insertable=false)
	private long responseId;
	
	@ManyToOne(cascade = CascadeType.DETACH, targetEntity=Response.class)
	private Response response;

	@Column(nullable = false, name = "QUESTION_ID", updatable=false, insertable=false)
	private long questionId;
	
	@ManyToOne(cascade = CascadeType.DETACH, targetEntity=Question.class)
	private Question question;

	@Column(nullable = false, name = "SURVEY_ID", updatable=false, insertable=false)
	private long surveyId;
	
	@ManyToOne(cascade = CascadeType.DETACH, targetEntity=Survey.class)
	private Survey survey;

}
