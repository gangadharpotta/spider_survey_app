package com.spiderlogic.qsystem.domain;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "surveys")
public class Survey {

	@Id
	@Column(nullable = false, name = "ID")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	@Column(nullable = false, name = "TITLE")
	private String title;

	@Column(nullable = false, name = "HELPTEXT")
	private String helptext;

	@Column(nullable = true, name = "CREATED_AT", updatable = false, insertable = false)
	private Date createdAt;

	@Column(nullable = false, name = "OWNER_ID", updatable = false, insertable = false)
	private long ownerId;

	@ManyToOne(cascade = CascadeType.DETACH, targetEntity = User.class)
	private User owner;

}
