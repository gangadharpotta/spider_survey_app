package com.spiderlogic.qsystem.domain;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity 
@Table (name = "responses")
public class Response {

	@Id
	@Column(nullable = false, name = "ID")	
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	
	@Column(nullable = true, name = "CREATED_AT", updatable=false, insertable=false)
	private Date createdAt;
	
	@Column(nullable = false, name = "SURVEY_ID", updatable=false, insertable=false)
	private long surveyId;
	
	@ManyToOne(cascade = CascadeType.DETACH, targetEntity=Survey.class)
	private Survey survey;

	@Column(nullable = false, name = "OWNER_ID", updatable=false, insertable=false)
	private long ownerId;
	
	@ManyToOne(cascade = CascadeType.DETACH, targetEntity=User.class)
	private User owner;
	
	@Column(nullable = false, name = "REFERENCE_ID", updatable=false, insertable=false)
	private long referenceId;
	
	@ManyToOne(cascade = CascadeType.DETACH, targetEntity=User.class)
	private User reference;
	
	@Column(nullable = false, name = "STATUS")
	private String status;


}
