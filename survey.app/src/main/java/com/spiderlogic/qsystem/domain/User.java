package com.spiderlogic.qsystem.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity 
@Table (name = "users")
public class User {

	@Id
	@Column(nullable = false, name = "ID")	
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	
	@Column(nullable = false, name = "LOGIN")
	private String login;

	@Column(nullable = false, name = "NAME")
	private String name;

	@Column(nullable = false, name = "IS_ADMIN")
	private boolean isAdmin;
	
	@Column(nullable = false, name = "PASSWORD")
	private String password;
	
	@Column(nullable = true, name = "CREATED_AT", updatable=false, insertable=false)
	private Date createdAt;


}
