-- MySQL dump 10.13  Distrib 5.6.21, for Win64 (x86_64)
--
-- Host: localhost    Database: qdb
-- ------------------------------------------------------
-- Server version	5.6.21-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `answers`
--

DROP TABLE IF EXISTS `answers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `answers` (
  `ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ANSWER` varchar(2000) DEFAULT NULL,
  `RESPONSE_ID` int(11) unsigned NOT NULL,
  `QUESTION_ID` int(11) unsigned NOT NULL,
  `SURVEY_ID` int(11) unsigned NOT NULL,
  `CREATED_AT` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `ID_UNIQUE` (`ID`),
  UNIQUE KEY `RESPONSE_QUESTION_ID_idx` (`RESPONSE_ID`,`QUESTION_ID`),
  KEY `ANSWER_RESPONSE_ID_idx` (`RESPONSE_ID`),
  KEY `ANSWER_QUESTION_ID_idx` (`QUESTION_ID`,`SURVEY_ID`),
  CONSTRAINT `ANSWER_QUESTION_ID` FOREIGN KEY (`QUESTION_ID`, `SURVEY_ID`) REFERENCES `questions` (`ID`, `SURVEY_ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `ANSWER_RESPONSE_ID` FOREIGN KEY (`RESPONSE_ID`) REFERENCES `responses` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `answers`
--

LOCK TABLES `answers` WRITE;
/*!40000 ALTER TABLE `answers` DISABLE KEYS */;
INSERT INTO `answers` VALUES (1,'ANS-1 : YES',5,1,1,'2014-12-03 21:43:14'),(2,'ANS-2 : NO',5,2,19,'2014-12-03 21:43:14'),(3,'ANS-1 : MAY BE ',6,1,1,'2014-12-03 21:43:14'),(4,'ANS-2 : NO',6,2,19,'2014-12-03 21:43:14'),(6,'ANS-1 : YES',5,3,19,'2014-12-03 22:52:52'),(8,'ANS-1 : YES',5,4,1,'2014-12-04 07:48:31');
/*!40000 ALTER TABLE `answers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary view structure for view `answers_view`
--

DROP TABLE IF EXISTS `answers_view`;
/*!50001 DROP VIEW IF EXISTS `answers_view`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `answers_view` AS SELECT 
 1 AS `ID`,
 1 AS `ANSWER`,
 1 AS `RESPONSE_ID`,
 1 AS `QUESTION_ID`,
 1 AS `SURVEY_ID`,
 1 AS `CREATED_AT`,
 1 AS `SURVEY_TITLE`,
 1 AS `QUESTION_TITLE`,
 1 AS `QUESTION_TYPE`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `questions`
--

DROP TABLE IF EXISTS `questions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `questions` (
  `ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `SURVEY_ID` int(11) unsigned NOT NULL,
  `TITLE` varchar(2000) NOT NULL,
  `HELPTEXT` varchar(2000) DEFAULT NULL,
  `TYPE` int(10) NOT NULL,
  `QUESTION_INDEX` int(10) unsigned DEFAULT NULL,
  `CREATED_AT` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `ID_UNIQUE` (`ID`),
  UNIQUE KEY `QUESTIONNAIRE_QUESTION_ID` (`ID`,`SURVEY_ID`),
  KEY `QUESTION_QUESTIONNAIRE_ID_idx` (`SURVEY_ID`),
  CONSTRAINT `QUESTION_SURVEY_ID` FOREIGN KEY (`SURVEY_ID`) REFERENCES `surveys` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `questions`
--

LOCK TABLES `questions` WRITE;
/*!40000 ALTER TABLE `questions` DISABLE KEYS */;
INSERT INTO `questions` VALUES (1,1,'Question 1 - Do you like your pet?','Question 1 Help Text',0,1,'2014-12-03 21:34:32'),(2,19,'Q-2',NULL,1,2,'2014-12-03 21:34:32'),(3,19,'Q-3',NULL,1,3,'2014-12-03 22:27:43'),(4,1,'Question 4 - Do you like your boss?','Question 4 Help Text',0,4,'2014-12-03 22:28:07'),(5,1,'Question 5 - Do you like your boss?','Question 4 Help Text',0,4,'2014-12-04 07:46:34'),(6,9,'Q1 NEW SURVEYS','',1,1,'2014-12-04 16:44:50'),(7,10,'TTTTTT','',1,1,'2014-12-04 16:45:27'),(8,16,'2015 Appraisal Survey - Question 1','',0,1,'2014-12-04 23:00:54'),(9,17,'First Question','',1,1,'2014-12-04 23:02:29'),(10,18,'On scale of 1 to 5 where do you rank your job satisfaction?','',0,1,'2014-12-04 23:36:10'),(11,19,'Q-1','',1,1,'2014-12-04 23:36:58');
/*!40000 ALTER TABLE `questions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `responses`
--

DROP TABLE IF EXISTS `responses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `responses` (
  `ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `CREATED_AT` datetime DEFAULT CURRENT_TIMESTAMP,
  `OWNER_ID` int(11) unsigned NOT NULL,
  `REFERENCE_ID` int(11) unsigned NOT NULL,
  `SURVEY_ID` int(11) unsigned NOT NULL,
  `STATUS` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `ID_UNIQUE` (`ID`),
  KEY `OWNER_ID_idx` (`OWNER_ID`),
  KEY `REFERENCE_ID_idx` (`REFERENCE_ID`),
  KEY `RESPONSE_QUESTIONNAIRE_ID_idx` (`SURVEY_ID`),
  CONSTRAINT `RESPONSE_SURVEY_ID` FOREIGN KEY (`SURVEY_ID`) REFERENCES `surveys` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `responses`
--

LOCK TABLES `responses` WRITE;
/*!40000 ALTER TABLE `responses` DISABLE KEYS */;
INSERT INTO `responses` VALUES (5,'2014-12-03 21:40:44',2,1,1,'PENDING'),(6,'2014-12-03 21:40:44',3,1,1,'DONE'),(11,'2014-12-03 22:50:01',2,1,1,'PENDING');
/*!40000 ALTER TABLE `responses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary view structure for view `responses_view`
--

DROP TABLE IF EXISTS `responses_view`;
/*!50001 DROP VIEW IF EXISTS `responses_view`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `responses_view` AS SELECT 
 1 AS `sender`,
 1 AS `title`,
 1 AS `ID`,
 1 AS `CREATED_AT`,
 1 AS `OWNER_ID`,
 1 AS `REFERENCE_ID`,
 1 AS `SURVEY_ID`,
 1 AS `STATUS`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `surveys`
--

DROP TABLE IF EXISTS `surveys`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `surveys` (
  `ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `TITLE` varchar(2000) NOT NULL,
  `CREATED_AT` datetime DEFAULT CURRENT_TIMESTAMP,
  `OWNER_ID` int(11) unsigned NOT NULL,
  `HELPTEXT` varchar(2000) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `ID_UNIQUE` (`ID`),
  KEY `OWNER_ID_idx` (`OWNER_ID`),
  CONSTRAINT `QUESTIONNAIRE_OWNER_ID` FOREIGN KEY (`OWNER_ID`) REFERENCES `users` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `surveys`
--

LOCK TABLES `surveys` WRITE;
/*!40000 ALTER TABLE `surveys` DISABLE KEYS */;
INSERT INTO `surveys` VALUES (1,'Questionnaire 1','2014-12-03 21:29:20',1,'Questionnaire 1 Help Text'),(2,'Questionnaire 2',NULL,1,'Questionnaire 2 Help Text'),(3,'Questionnaire 3','2014-12-03 22:02:31',1,'Questionnaire 3 Help Text'),(4,'Questionnaire 4','2014-12-04 07:44:57',1,'Questionnaire 4 Help Text'),(5,'Questionnaire 5','2014-12-04 13:26:31',1,'Questionnaire 5 Help Text'),(6,'Questionnaire 6','2014-12-04 15:35:32',1,'Questionnaire 6 Help Text'),(7,'TEST1','2014-12-04 16:41:51',1,'TEST2'),(8,'TEST1','2014-12-04 16:42:17',1,'TEST2'),(9,'MY NEW SURVEY','2014-12-04 16:44:25',1,'XXXXXXXXXXXXXXX'),(10,'TTTT','2014-12-04 16:45:02',1,'TTTTTTTTT'),(11,'Appraisal 2014','2014-12-04 22:45:48',1,'Appraisal 2014'),(12,'Appraisal 2014','2014-12-04 22:46:05',1,'Appraisal 2014'),(13,'Appraisal 2014','2014-12-04 22:47:50',1,'Appraisal 2014'),(14,'2015 Appraisal Survey','2014-12-04 22:58:02',1,'2015 Appraisal Survey'),(15,'2015 Appraisal Survey','2014-12-04 22:59:35',1,'2015 Appraisal Survey'),(16,'2015 Appraisal Survey','2014-12-04 23:00:46',1,'2015 Appraisal Survey'),(17,'My Brand New Survey','2014-12-04 23:02:27',1,'...'),(18,'Employment Survey 2015','2014-12-04 23:36:10',1,'Employment Survey 2015'),(19,'Useless Survey!!? Just for Test','2014-12-04 23:36:58',1,'...');
/*!40000 ALTER TABLE `surveys` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary view structure for view `surveys_view`
--

DROP TABLE IF EXISTS `surveys_view`;
/*!50001 DROP VIEW IF EXISTS `surveys_view`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `surveys_view` AS SELECT 
 1 AS `ID`,
 1 AS `TITLE`,
 1 AS `CREATED_AT`,
 1 AS `OWNER_ID`,
 1 AS `HELPTEXT`,
 1 AS `OWNER_NAME`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `NAME` varchar(45) NOT NULL,
  `LOGIN` varchar(45) NOT NULL,
  `PASSWORD` varchar(2000) NOT NULL,
  `IS_ADMIN` smallint(1) NOT NULL,
  `CREATED_AT` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `LOGIN_UNIQUE` (`LOGIN`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'Admin User','admin','password',1,'2014-12-03 21:35:34'),(2,'Test User 1','test1','password',0,'2014-12-03 21:35:34'),(3,'Test User 2','test2','password',0,'2014-12-03 21:39:26'),(4,'Test User 4 - Modified','test4','password',0,'2014-12-04 13:20:48'),(5,'newuser1','newuser1@spiderlogic.com','password',0,'2014-12-04 21:10:04'),(6,'newuser2','newuser2@yahoo.com','password',0,'2014-12-04 21:18:09'),(7,'uadtani','uadtani@spiderlogic.com','password',0,'2014-12-04 23:33:30');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Final view structure for view `answers_view`
--

/*!50001 DROP VIEW IF EXISTS `answers_view`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `answers_view` AS select `a`.`ID` AS `ID`,`a`.`ANSWER` AS `ANSWER`,`a`.`RESPONSE_ID` AS `RESPONSE_ID`,`a`.`QUESTION_ID` AS `QUESTION_ID`,`a`.`SURVEY_ID` AS `SURVEY_ID`,`a`.`CREATED_AT` AS `CREATED_AT`,`s`.`TITLE` AS `SURVEY_TITLE`,`q`.`TITLE` AS `QUESTION_TITLE`,`q`.`TYPE` AS `QUESTION_TYPE` from ((`answers` `a` join `surveys_view` `s` on((`s`.`ID` = `a`.`SURVEY_ID`))) join `questions` `q` on((`q`.`ID` = `a`.`QUESTION_ID`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `responses_view`
--

/*!50001 DROP VIEW IF EXISTS `responses_view`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `responses_view` AS select `u`.`NAME` AS `sender`,`s`.`TITLE` AS `title`,`r`.`ID` AS `ID`,`r`.`CREATED_AT` AS `CREATED_AT`,`r`.`OWNER_ID` AS `OWNER_ID`,`r`.`REFERENCE_ID` AS `REFERENCE_ID`,`r`.`SURVEY_ID` AS `SURVEY_ID`,`r`.`STATUS` AS `STATUS` from ((`responses` `r` join `surveys` `s` on((`s`.`ID` = `r`.`SURVEY_ID`))) join `users` `u` on((`u`.`ID` = `r`.`REFERENCE_ID`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `surveys_view`
--

/*!50001 DROP VIEW IF EXISTS `surveys_view`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `surveys_view` AS select `s`.`ID` AS `ID`,`s`.`TITLE` AS `TITLE`,`s`.`CREATED_AT` AS `CREATED_AT`,`s`.`OWNER_ID` AS `OWNER_ID`,`s`.`HELPTEXT` AS `HELPTEXT`,`u`.`NAME` AS `OWNER_NAME` from (`surveys` `s` join `users` `u` on((`u`.`ID` = `s`.`OWNER_ID`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-12-05  0:49:31
