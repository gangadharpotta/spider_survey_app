(function(){
	'use strict';
	
	angular.module('survey-app')
		.controller('InboxController', ['MyConfig', '$rootScope', '$scope', '$http', '$location', '$resource', '$state', '$stateParams', inboxController]);
	
	function inboxController(MyConfig, $rootScope, $scope, $http, $location, $resource, $state, $stateParams) {
		$rootScope.UserId = 2;
		
		var SurveyResponse = $resource(MyConfig.URL_GET_ALL_SURVEY_RESPONSES_BY_USER, {ownerId:'@id'});
		SurveyResponse.get({ownerId: $rootScope.UserId}, function(surveys){
			$rootScope.surveyResponses = surveys._embedded.responseviews;
			
			$rootScope.filteredSurveyResponses = $rootScope.surveyResponses;
		});
		
		$scope.$watch("searchResponsePattern", function(){
            if($scope.searchResponsePattern!=null && $scope.searchResponsePattern.length > 0){
                $rootScope.filteredSurveyResponses = $.grep($rootScope.surveyResponses, function(surveyResponse){
                    return(surveyResponse.sender.toLowerCase().indexOf($scope.searchResponsePattern.toLowerCase())>=0 || surveyResponse.title.toLowerCase().indexOf($scope.searchResponsePattern.toLowerCase())>=0);
                });
            }else{
                $rootScope.filteredSurveyResponses = $rootScope.surveyResponses;
            }
        });
		
//		$scope.onResponceClick = function(surveyResponse){
//			
////			alert($state);
//			
//			if (surveyResponse.status == 'PENDING') {
//				var SurveyQuestions = $resource(MyConfig.URL_GET_ALL_QUESTIONS_BY_SURVEY_ID, {surveyId:'@id'});
//				SurveyQuestions.get({surveyId: surveyResponse.surveyId}, function(surveyQuestions){
//					$rootScope.surveyQuestions = surveyQuestions._embedded.questions;
//					$rootScope.SurveyTitle = surveyResponse.title;
//				});
//								
////				$location.path('/responses/edit/' + surveyResponse.id);
//				$state.go('.surveyResponseView', $stateParams);
//			}
//			else if (surveyResponse.status == 'COMPLETE') {
//				var SurveyAnswers = $resource(MyConfig.URL_GET_ALL_ANSWERS_BY_SURVEY_RESPONSE_ID, {responseId:'@id'});
//				SurveyAnswers.get({responseId: surveyResponse.id}, function(surveyAnswers){
//					$rootScope.surveyAnswers = surveyAnswers._embedded.answers;
//					$rootScope.SurveyTitle = surveyResponse.title;
//				});
//				
//				$location.path('/responses/view/' + surveyResponse.id);
//			}
//		};
		
		$scope.onCancelSurveyResponse = function(){
			$rootScope.surveyQuestions = [];
			$location.path('/welcome');
		};
	};
		
})();
