﻿
(function () {

    angular.module('survey-app').controller("SurveyDesignerController", ['MyConfig', '$scope','$http', '$location', surveyDesignerController]);

    function surveyDesignerController(MyConfig, $scope, $http, $location) {

        $scope.questionnaire = [{ "title": "", "helptext": "",  "type": "" ,"index":"1","survey":""}];


        $scope.QuestionTypes = [{ "Type": "Scale", "Value": 0 }, { "Type": "Text", "Value": 1 }];



        $scope.Survey = { "title": "", "helptext": "", "owner": "/users/1" };

        $scope.OnRowAdd = function () {

            var count = $scope.questionnaire.length + 1;
            $scope.questionnaire.push({ "title": "", "id": count ,"index":count});
        }

        $scope.OnRowRemove = function (itemToRemove) {
            var index = $scope.questionnaire.indexOf(itemToRemove);
            $scope.questionnaire.splice(index, 1);
        }

        $scope.OnSave = function () {

             $scope.createSurvey();

        }

        $scope.OnCancel = function () {

            $location.path("/welcome");

       }

        $scope.createSurvey = function () {
            $http.post(MyConfig.URL_SURVEYS, $scope.Survey).
                success(function(data, status, headers, config) {
                	var restLocation = headers("Location");
                    var surveyId = restLocation.substring(restLocation.lastIndexOf("/")+1);
                    for (var i = 0; i < $scope.questionnaire.length; i++) {
                        var question = $scope.questionnaire[i];
                        question.survey = "/survey/" + surveyId;
                        $scope.CreateQuestion(question);
                    }
                    
                    $location.path("/welcome");
                   
                }).
                error(function(data, status, headers, config) {

                });
        }
        
        $scope.CreateQuestion=function(question) {
            $http.post(MyConfig.URL_QUESTIONS, question).
                success(function(data, status, headers, config) {
                    
                }).
                error(function(data, status, headers, config) {

                });
        }   
    }

})();