(function () {
    'use strict';

    angular.module('survey-app')
        .controller('ControllerName', ['$rootScope', '$scope', '$http', '$location', controllerFunction]);

    function controllerFunction($rootScope, $scope, $http, $location) {

        $scope.controllerName = "Controller Name Working..!";
        
        function init() {
        	//initialization code goes here...
        }

        init();
    };
})();