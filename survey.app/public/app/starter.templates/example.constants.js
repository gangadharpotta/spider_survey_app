var myAppModule = angular.module('myAngularApp');


	myAppModule.constant('MyConfig', {
		GET_POLICIES_URL: 	'/mocked/policies.json.txt',
		GET_TODOS_URL: 	'/mocked/todos.json.txt',
		GET_CUSTOMERS_URL: 	'/mocked/customers.json.txt',
		GET_QUOTES_URL: 	'/mocked/quotes.json.txt'
});




