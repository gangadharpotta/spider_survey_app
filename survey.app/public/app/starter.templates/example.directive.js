(function () {
    'use strict';

    angular.module('shoppingApp')
        .directive('myResult', [myResult]);

    function myResult() {
    	 return {
    	 	restrict: 'E',
    	 	scope : {
    	 		items : '=',
    	 		onRowSelect : '&'
    	 	},
		    templateUrl: '/result-image-template.html'
		 };
    };
})();