(function() {
    'use strict';

    angular.module('survey-app')
        .factory('usersService', ['$rootScope', '$resource', '$q', usersService]);

    var usersUrl = '/rest/users';
    var usersByLoginUrl = 'http://sl-pn-lt-0075:8080/rest/users/search/findByLogin?:login';
    
    function usersService($rootScope, $resource, $q, settings, customersService, lookupDataService) {
        return {
            getUsers : getUsers,
            getUserByLogin : getUserByLogin
        };

        function getUsers() {
            return $resource(usersUrl, { userId: userId }).get().$promise;
        };
        
        function getUserByLogin(login) {
            return $resource(usersByLoginUrl, { login: login }).get().$promise;
        };

})();
