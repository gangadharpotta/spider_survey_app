(function(){

	"use strict";

    angular.module("survey-app")
        .factory("registrationService", ["$http", "MyConfig", registrationService]);

    function registrationService($http, config) {
		return {
			register: function(userInfo){
				return $http.post(config.URL_CREATE_USER, userInfo);
			}
		};
    }

})();