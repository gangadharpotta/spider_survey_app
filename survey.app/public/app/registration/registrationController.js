(function(){

	"use strict";
	
	angular.module("survey-app")
        .controller("registrationController", ["$scope", "$http", "$location", "registrationService", registrationController]);
		
	function registrationController($scope, $http, $location, service){
		$scope.passwordsMatch = function(){
			if(!$scope.user) return true;
			
			return $scope.user.confirmPassword == $scope.user.password;
		};
		
		$scope.register = function(){
//			if(registrationForm.$valid){
				var user = $scope.user;
				var postData = {
					login: user.email,
					name: user.name,
					password: user.password,
					admin: false
				};
				
				service.register(postData)
					.success(function(data, status, headers, config){
//						alert(JSON.stringify(data));
						$location.path("/login")
					});
//			}
		};
		
		$scope.cancelRegister = function(){
			$location.path("/login")
		};
	}

})();