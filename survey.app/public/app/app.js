'use strict';

/**
 * @ngdoc overview
 * @name myappApp
 * @description
 * # myappApp
 *
 * Main module of the application.
 */
/*angular
  .module('survey-app', [
    'ngRoute', 'ngResource', 'ui.router'
  ])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/login', {
        templateUrl: '/app/login/views/login.html'
      })
      .when('/welcome', {
        templateUrl: '/app/welcome/welcome.html'
      })
      .when('/register', {
        templateUrl: '/app/registration/register.html'
      })
      .when('/surveys', {
        templateUrl: '/app/SurveyRepository/surveyrepository.html',
        controller:'SurveyRepositoryController'
      })
      .when('/surveys/new', {
        templateUrl: '/app/SurveyDesigner/SurveryDesigner.html'
      })
      .when('/surveys/edit/:surveyId', {
        templateUrl: '/app/SurveyDesigner/surveydesigner.html'
      })
      .when('/surveys/send/:surveyId', {
        templateUrl: '/app/SurveyMailer/surveymailer.html'
      })
      .when('/responses/edit/:responseId', {
        templateUrl: '/app/inbox/views/surveyResponseEdit.html'
      })
      .when('/responses/view/:responseId', {
        templateUrl: '/app/inbox/views/surveyResponseView.html'
      })
      .otherwise({
        redirectTo: '/'
      });
  })
  .config(function($stateProvider, $urlRouterProvider){
	  $urlRouterProvider.otherwise('/welcome');
	  
	  $stateProvider
	      .state('welcome', {
	          url: '/welcome',
	          templateUrl: '/app/welcome/welcome.html'
	      })	      
	      .state('inbox', {
	    	  url: '/inbox',
	          templateUrl: '/app/inbox/views/inbox.html'
	      })
	      .state('survey-repository', {
	    	  url: '/survey-repository',
	          templateUrl: '/app/SurveyRepository/surveyrepository.html'
	      });
  });
 */

angular
.module('survey-app', [
  'ngResource', 'ui.router'
])
.config(function($stateProvider, $urlRouterProvider){
	  $urlRouterProvider.otherwise('/login');
	  
	  $stateProvider
		  .state('login', {
	          url: '/login',
	          templateUrl: '/app/login/views/login.html'
	      })
	      .state('register', {
	          url: '/register',
	          templateUrl: '/app/registration/register.html'
	      })
	      .state('welcome', {
	          url: '/welcome',
	          templateUrl: '/app/welcome/welcome1.html',
	          controller: ['$state', '$stateParams',
	               function($state, $stateParams){
	        	  		$state.go('welcome.inbox');
	          }]
	      })	      
	      .state('welcome.inbox', {
	    	  url: '/inbox',
	    	  views: {
	    		  '':{
	    			  templateUrl: '/app/inbox/views/inbox.html'
	    		  }
	    	  }
	      })
	      .state('welcome.surveyRepository', {
	    	  url: '/surveyRepository',
	          templateUrl: '/app/SurveyRepository/surveyrepository.html'
	      })
	      .state('welcome.inbox.surveyResponseView', {
	    	  url: '/responses/view/:surveyResponseId',
	          views: {
	        	  '@welcome': {
	        		  templateUrl: '/app/inbox/views/surveyResponseView.html',
	        		  controller: ['MyConfig', '$rootScope', '$scope', '$stateParams', '$state', '$resource',
	    			       function(MyConfig, $rootScope, $scope, $stateParams, $state, $resource){
			      				var surveyResponseId = $stateParams.surveyResponseId;
	        			  		
	        			  		var SurveyAnswers = $resource(MyConfig.URL_GET_ALL_ANSWERS_BY_SURVEY_RESPONSE_ID, {responseId:'@id'});
			      				SurveyAnswers.get({responseId: surveyResponseId}, function(surveyAnswers){
			      					$rootScope.surveyAnswers = surveyAnswers._embedded.answers;
//			      					$rootScope.SurveyTitle = surveyResponse.title;
			      				});
	    			  }]
	        	  }
	          }
	      })
	      .state('welcome.surveyRepository.create', {
	    	  url: '/surveys/new',
	    	  views: {
	    		  '@welcome': {
	    			  templateUrl: '/app/SurveyDesigner/SurveryDesigner.html',
	        		  controller: ['MyConfig', '$rootScope', '$scope', '$stateParams', '$state', '$resource',
	    			       function(MyConfig, $rootScope, $scope, $stateParams, $state, $resource){
			      				
	    			  }]
	        	  }
	    	  }
	      });
})
.run(function($rootScope, $state){
	$rootScope.$state = $state;
});


angular.module('survey-app')
    .controller('MainController', ['$location','$rootScope', '$scope', mainController]);

function mainController($location, $rootScope, $scope) {
	$scope.signOut = function(){
		$rootScope.user = null;
		$location.path("/login");
	}
	
	if($rootScope.user == null){
		$location.path("/login");
	}else{
		$location.path("/welcome");
	}
}