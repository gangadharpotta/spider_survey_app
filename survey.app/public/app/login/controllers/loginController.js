(function () {

    var loginController = function ($rootScope, $scope, $http, MyConfig, $location) {

    	if($rootScope.user != null){
    		$location.path("/welcome");
    	}
    	
    	$scope.signUp = function(){
    		$location.path("/register");
    	}
    	
        $scope.validateUser = function () {
            $http.get(MyConfig.URL_GET_USER_BY_LOGIN,  {
                params: {
                    login: $scope.user.login
                }
            }).success(function (data, status, headers, config) {
                if (!data._embedded)
                    alert('User does not exist or password did not match');
                else if (data._embedded.users[0].password != $scope.user.password)
                    alert('Login password does not match');
                else if (data._embedded.users[0].password == $scope.user.password) {
//                    alert("logged in successfully");
                    $rootScope.user = data._embedded.users[0];
                    $location.path("/welcome");
                }
                
            });
        };
        
    };

    angular.module('survey-app').controller("loginController", loginController);
}());