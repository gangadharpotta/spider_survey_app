(function() {

    var loginService= function($resource,MyConfig) {
       return {
            getByLogin : function(loginId) {        
                return $resource(MyConfig.URL_GET_ALL_USERS_BY_LOGIN,[loginId]).get();
            }
       }; 
    };
              
    angular.module('survey-app').service("loginService",loginService);
       
})();
    