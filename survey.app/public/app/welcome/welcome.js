(function () {
    'use strict';

    angular.module('survey-app')
        .controller('WelcomeController', ['MyConfig', '$rootScope', '$scope', '$location', '$resource', controllerFunction]);


    function controllerFunction(MyConfig, $rootScope, $scope, $location, $resource) {
    	
	    $("#myTab a").click(function(e){
	    	e.preventDefault();
	    	$(this).tab('show');
	    });
    	
        function init() {
        	//initialization code goes here...
        	$scope.users = $resource(MyConfig.URL_GET_ALL_USERS).get();
        }
    	
        init();
    };
})();