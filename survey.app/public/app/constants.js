var myAppModule = angular.module('survey-app');

	//survey-app.localhost.com => Must Point to Server Where APIs are running.
	//Make sure that your C:\WINDOES\SYSTEM32\drivers\etc\hosts has entry for mapping this alias to valid API server host
	myAppModule.constant('MyConfig', {
		URL_GET_ALL_USERS: 	'http://survey-app.localhost.com:8080/rest/users',
		URL_GET_ALL_USERS_BY_LOGIN : 'http://survey-app.localhost.com:8080/rest/users/search/findByLogin?:login',
		URL_SURVEYS : 'http://survey-app.localhost.com:8080/rest/surveys',
		URL_SURVEY_VIEWS : 'http://survey-app.localhost.com:8080/rest/surveyviews',
		URL_QUESTIONS : 'http://survey-app.localhost.com:8080/rest/questions',
		URL_CREATE_USER : 'http://survey-app.localhost.com:8080/rest/users',
			URL_GET_ALL_SURVEY_RESPONSES_BY_USER: 'http://survey-app.localhost.com:8080/rest/responseviews/search/findByOwnerId?ownerId=:ownerId',
//		URL_GET_ALL_SURVEY_RESPONSES_BY_USER: '/mocked.server/surveyresponses.json',
			URL_GET_ALL_QUESTIONS_BY_SURVEY_ID: 'http://survey-app.localhost.com:8080/rest/questions/search/findBySurveyId?surveyId=:surveyId',
//		URL_GET_ALL_QUESTIONS_BY_SURVEY_ID: '/mocked.server/surveyquestions.json',
		URL_GET_ALL_ANSWERS_BY_SURVEY_RESPONSE_ID: 'http://survey-app.localhost.com:8080/rest/answers/search/findByResponseId?responseId=:responseId',
//		URL_GET_ALL_ANSWERS_BY_SURVEY_RESPONSE_ID: '/mocked.server/surveyanswers.json',
		URL_GET_USER_BY_LOGIN : 'http://survey-app.localhost.com:8080/rest/users/search/findByLogin'
});




