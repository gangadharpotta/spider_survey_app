(function () {
    'use strict';

    angular.module('survey-app')
        .controller('SurveyRepositoryController', ['MyConfig', '$rootScope', '$scope', '$http', '$location', surveyrepositoryController]);

    function surveyrepositoryController(MyConfig, $rootScope, $scope, $http, $location) {

        //$scope.surveys = $resource('http://sl-pn-lt-0075:8080/rest/surveys').get();

        //$scope.surveys = $http.get('http://sl-pn-lt-0075:8080/rest/surveys').$promise;

        function loadSurveys() {
            $http.get(MyConfig.URL_SURVEY_VIEWS).success(function (response) {
                
                $scope.surveys = response._embedded.surveyviews;
                console.log($scope.surveys.length + " results found!");
	            //$location.path("/results");
            });
        };

        $scope.onNewSurveyClick = function() {
            console.log("onNewSurveyClick");
            $location.path("/surveys/new");
        };

        loadSurveys();

    };
})();